# project name
projectname = "losbuilder-lab"

# OS image
sourceimage = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"

# the base image is the source image for all VMs created from it
baseimagediskpool = "default"


# domain and network settings
domainname  = "local"
networkname = "default" # default==NAT

subnets       = ["10.26.4.0/24"]
network_names = ["internal"]
dhcp          = [true]
mode          = ["bridge"]

# host-specific settings
# RAM in bytes
# disk size in bytes (disk size must be greater than source image virtual size)
hosts = {
  "builder" = {
    name     = "builder",
    vcpu     = 4,
    memory   = 1024 * 12,
    diskpool = "default",
    disksize = 1024 * 1024 * 1024 * 150,
    mac      = "02:26:00:13:37:00",
    # sourceimage = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img",
    sourceimage = "/var/lib/libvirt/images/focal-server-cloudimg-amd64.img",
    category    = "losbuilder-lab",
    network = {
    },
  },
}

