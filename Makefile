ap = ansible-playbook
ausr = ansible
privk = ~/.ssh/tf-ansible
pbdir = playbooks
pbname = losbuilder-lab.yml

.PHONY: ans infra destroy

ans:
	$(ap) -u $(ausr) --private-key $(privk) -i hosts $(pbdir)/$(pbname)

infra:
	cd vms; make infra

destroy:
	cd vms; make destroy
